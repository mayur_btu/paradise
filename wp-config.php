<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'paradise');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'SwLt?_L0*7iI>h)9UBWF*MiL.%;{0/W@-Yfnkzrf2!oD.b9y:;Nzc>[B%TAp4<!)');
define('SECURE_AUTH_KEY',  '7@?(VYzhVZ*NXiZ!Ad]S?6ej5NAE`nozY`<1PnL#@L[Gq4U:C0SD1:n]fSF)K3^R');
define('LOGGED_IN_KEY',    '+nx.uthEgDJ <@Nax3w7]EPvpK[3Iz5R/`7l-#~3CqV(P7_v#,aaQ*Nw%<}{gA/X');
define('NONCE_KEY',        ').~R=myRaxYPOq010;:RrA:Vt%{Z)^:c^)MfkB@6/.G;a.QQ:h_r5>go^@| I[xl');
define('AUTH_SALT',        'zrWBzk+b3jz|:h.}oK%?%0eIF]=N@k%F<LzyM|8+XI-q`/?`>s6m4`K;;W%#@ds+');
define('SECURE_AUTH_SALT', 'SE/kC8P|o/uf05CXh`ff><kNZW+n6#46CULZWz9Ep2$e9#0NFCY G6PhOX3C82^%');
define('LOGGED_IN_SALT',   'pz2e{l(NWk?$*LC~C^s}K`[ohB@9}};:+(h!X9u]9H78jfGMR|X=GFZb6m6#M1sK');
define('NONCE_SALT',       '?,.g#<B&fKSlHjTELi^c0ccxH:}72Cv@JS4B@4WZ#<Gh%X[KZkD?|8o1V}?zXH35');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
