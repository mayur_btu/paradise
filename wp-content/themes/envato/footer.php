<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>


	</section>
				<!--// Contact Area -->

				<!-- Footer Area -->
				<footer id="footer-area" class="footer-area section-padding-xs bg-dark">
					<div class="container">
						<div class="row">
							<div class="col-lg-6">
								<ul class="footer-social-links">
									<li><a href="#">Facebook</a></li>
									<li><a href="#">Twitter</a></li>
									<li><a href="#">Google+ </a></li>
									<li><a href="#">Linkedin </a></li>
									<li><a href="#">Behance </a></li>
								</ul>
							</div>
							<div class="col-lg-6">
								<div class="footer-copyright">
									<p>Copyright &copy; <a href="#">Photoghor</a> 2018. All Right Reserved</p>
								</div>
							</div>
						</div>
					</div>
				</footer>
				<!--// Footer Area -->

			</div>

		</div>

	</div>
	<!--// Wrapper -->

	<!-- Js Files -->
	<script src="<?php bloginfo('template_url'); ?>/assets/js/vendor/modernizr-3.6.0.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/vendor/jquery-3.3.1.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/popper.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/plugins.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/main.js"></script>
<?php wp_footer(); ?>

</body>
</html>
