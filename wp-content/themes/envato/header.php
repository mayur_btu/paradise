<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<!-- Favicon -->
	<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/assets/img/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/assets/img/icon.png">

<!-- Google font (font-family: 'IBM Plex Sans', sans-serif;) -->
	<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

	<!-- Plugins -->
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/plugins.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/style.css">

	<!-- <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/font-awesome.min.css"> -->
	<!-- <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/meanmenu.css"> -->
	<!-- <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/slick.min.css"> -->
	<!-- <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/lightgallery.min.css"> -->

	<!-- Custom Styles -->
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/custom.css">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="wrapper" class="wrapper">

		<span id="home"></span>

		<!-- Header -->
		<header class="header sticky-header fixed-header">
			<div class="container">
				<div class="header-inner d-none d-lg-flex">
					<div class="header-logo">
						<a href="index.html">
							<img src="<?php bloginfo('template_url'); ?>/assets/img/logo/logo-light.png" alt="logo">
						</a>
					</div>
					<div class="header-navigation">
						<button class="header-navigation-trigger">
							<span></span>
							<span></span>
							<span></span>
						</button>
						<nav class="main-navigation">
							<ul class="section-name">
								<li data-menuanchor="first-section" class="active"><a href="#first-section">Welcome</a></li>
								<li data-menuanchor="second-section"><a href="#second-section">About</a></li>
								<li data-menuanchor="third-section"><a href="#third-section">Portfolios</a></li>
								<li data-menuanchor="fourth-section"><a href="#fourth-section">Testimonials</a></li>
								<li data-menuanchor="fifth-section"><a href="#fifth-section">Contact</a></li>
							</ul>
						</nav>
					</div>
				</div>
				<div class="mobile-menu-wrapper d-block d-lg-none">
					<div class="mobile-menu clearfix">
						<a href="index.html" class="mobile-logo">
							<img src="<?php bloginfo('template_url'); ?>/assets/img/logo/logo-light.png" alt="mobile logo">
						</a>
					</div>
				</div>
			</div>
		</header>
		<!--// Header -->

		<!-- Fullpage Area Name -->
		<ul id="section-name" class="section-name">
			<li data-menuanchor="first-section" class="active"><a href="#first-section">Welcome</a></li>
			<li data-menuanchor="second-section"><a href="#second-section">About</a></li>
			<li data-menuanchor="third-section"><a href="#third-section">Portfolios</a></li>
			<li data-menuanchor="fourth-section"><a href="#fourth-section">Testimonials</a></li>
			<li data-menuanchor="fifth-section"><a href="#fifth-section">Contact</a></li>
		</ul>
		<!--// Fullpage Area Name -->

		<div id="fullpage">

			<div class="section" data-anchor="first-section">

				<!-- Hero Area -->
				<div class="hero-area">

					<div class="hero-slider hero-silder-active">

						<!-- Hero Area Single Slide -->
						<div class="hero-single-slide bg-image-1">
							<div class="container">
								<div class="hero-slide-text">
									<h1>Stay With Your <br>Dream & Passion</h1>
									<p>Photography is about capturing <br> souls not smiles </p>
								</div>
							</div>
						</div>
						<!--// Hero Area Single Slide -->

						<!-- Hero Area Single Slide -->
						<div class="hero-single-slide bg-image-2">
							<div class="container">
								<div class="hero-slide-text">
									<h1>Stay With Your <br>Dream & Passion</h1>
									<p>Photography is about capturing <br> souls not smiles </p>
								</div>
							</div>
						</div>
						<!--// Hero Area Single Slide -->

						<!-- Hero Area Single Slide -->
						<div class="hero-single-slide bg-image-3">
							<div class="container">
								<div class="hero-slide-text">
									<h1>Stay With Your <br>Dream & Passion</h1>
									<p>Photography is about capturing <br> souls not smiles </p>
								</div>
							</div>
						</div>
						<!--// Hero Area Single Slide -->

					</div>

					<div class="hero-area-outer">
						<div class="container">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-12">
									<div class="social-icons">
										<ul>
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
											<li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-12">
									<div class="hero-pagination"></div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<!--// Hero Area -->

			</div>

			<div class="section" data-anchor="second-section">
